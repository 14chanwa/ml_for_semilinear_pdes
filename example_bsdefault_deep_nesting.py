##
# Example use of dnesting on a Black-Scholes-Barenblatt equation
# From https://arxiv.org/abs/1809.07609
#

import numpy as np
import tensorflow as tf
import deep_nesting as dbsde
import time
import os
import matplotlib.pyplot as plt


####
# In this example, we solve a Black-Scholes with default risk equation using our new algorithm.
# Use example: 
# python example_bsbarenblatt_deep_nesting.py
# Warning: this uses a lot of memory. Decrease sp.n_inner if full
####


monte_carlo_size = 1024
min_decrease_rate = 0.05

d=100
case_name = "BlackScholesDefaultRisk"


T = 1.
X0 = 100 * np.ones((d,), dtype=np.float64)
n_timesteps=100

bse = dbsde.BlackScholesDefaultRisk(
        delta=2./3.,
        R=0.02,
        mu_bar=0.02,
        sigma_bar=0.2,
        v_h=50.,
        v_l=70.,
        gamma_h=0.2,
        gamma_l=0.02
        )

print("----- Solving BlackScholesDefaultRisk -----")

sp = dbsde.XavierSolver.Parameters(
        bse,
        d=d,
        T=T,
        n_timesteps=n_timesteps,
        X0=X0,
        Yini=[.7, 1.3]
        )

sp.valid_size = monte_carlo_size

sp.learning_rate = 0.01
sp.use_predefined_learning_rate_strategy = False
sp.n_miniter = 10000#2000
sp.n_maxiter = 10000#2000

def get_learning_rate(m):
    if m < 500:
        return 0.01
    elif m < 1000:
        return 0.005
    elif m < 1500:
        return 0.001
    return 0.0005

sp.get_learning_rate = get_learning_rate

sp.batch_size = 300
sp.valid_size = 300
sp.normalize_input_X = True

sp.num_hidden_layers = 3
sp.hidden_layer_size = 2*d#3*d
sp.activation_fn = tf.nn.tanh

sp.n_inner = 4000
sp.lamb = 1.0
sp.t_distrib_a = 0.0#0.0 # 0.2

# Call the solver which does not use DU
solver = dbsde.XavierSolver_NoDU(sp, base_name=case_name, output_directory="./output/test_deep_xavier_bsdefault/")

solver.train(run_name="Test", min_decrease_rate=min_decrease_rate)
