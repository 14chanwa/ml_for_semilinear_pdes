##
# Implements PDEs for DBSDE solvers
# From https://arxiv.org/abs/1809.07609
#

import abc

import numpy as np
import tensorflow as tf
import scipy.integrate as integrate

import multiprocessing
from joblib import Parallel, delayed

import hashlib

# The following two arguments are used when computing MC solutions
_MULTIPROCESSING_CORE_COUNT = 2
_NUM_MC_RANDOM_SAMPLES = 10000


##
# @brief Abstract base class for a semilinear PDE.
#
# The semilinear PDE should be defined by
# u_t + mu * grad(u) + 1/2 * Tr(sigma * sigma^T * Hess(u)) + f = 0
# with final condition u(T, x) = g(x), where diff_np is mu, vol_np is
# sigma, f_tf is f, g_tf is g.
#
# IMPORTANT: in the following, the function f_tf take Z as argument,
# where Z := sigma^T Grad u. The function f_tf_grad take as argument grad,
# which is Grad u.
# If provided, the algorithm will use f_tf_grad. If the function is not defined,
# it will use f_tf: make sure your function definitions are coherent. 
class SemilinearPDE(abc.ABC):
	
	@abc.abstractmethod
	##
	# @brief Defines the diffusion function for the PDE (NumPy format) -
	# prefer overriding this function with get_next in case an explicit
	# solution is known.
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param delta_t Time interval to the next state.
	#
	# @return The diffusion function: diff * delta_t.
	def diff_np(self, t, X, delta_t):
		pass
	
	@abc.abstractmethod
	##
	# @brief Defines the volatility function for the PDE (NumPy format) -
	# prefer overriding this function with get_next in case an explicit
	# solution is known.
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param delta_t Brownian motion to the next state.
	#
	# @return The diffusion function: vol.dot(delta_b).
	def vol_np(self, t, X, delta_b):
		pass
	
	@abc.abstractmethod
	##
	# @brief Defines the volatility function for the PDE (TensorFlow format) -
	# prefer overriding this function with get_next in case an explicit
	# solution is known.
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param delta_t Brownian motion to the next state.
	#
	# @return The diffusion function: vol.dot(delta_b).
	def vol_tf(self, t, X, delta_b):
		pass
		
	@abc.abstractmethod
	##
	# @brief Defines the nonlinear function (tf.Tensor format).
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param Y Current Y.
	# @param Z Current Z.
	#
	# @return The nonlinear function's tensor.
	def f_tf(self, t, X, Y, Z):
		pass
		
	@abc.abstractmethod
	##
	# @brief Defines the final condition function (tf.Tensor format).
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	#
	# @return The final condition function's tensor.
	def g_tf(self, t, X):
		pass
	
	@abc.abstractmethod
	##
	# @brief Defines the final condition function (NumPy array format).
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	#
	# @return The final condition function's tensor.
	def g_np(self, t, X):
		pass
	
	##
	# @brief Compute X_{t+1} from arguments (in NumPy format).
	# 
	# @param self
	# @param t Time
	# @param X X_t
	# @param delta_t {t+1} - t
	# @param delta_b B_{t+1}-B_t
	#
	# @return X_{t+1}
	def get_next(self, t, X, delta_t, delta_b):
		return X + self.diff_np(t, X, delta_t) + self.vol_np(t, X, delta_b)
	
	##
	# @brief Generate paths from initial conditions with provided integration parameters.
	# 
	# @param self
	# @param X0 Initial condition.
	# @param delta_t {t+1} - t
	# @param n_path_count Number of paths to be generated.
	# @param n_step Number of integration steps.
	# @param t0 Initial time.
	#
	# @return X Sample paths.
	# @return dB Sample Brownian motion.
	def generate_paths(self, X0, delta_t, n_path_count, n_step, t0=0, seed=None):
		n_dim = X0.size
		X = np.zeros((n_path_count, n_dim, n_step+1), dtype=np.float32)
		X[:, :, 0] = X0
		
		# Generate dB
		if seed is not None:
			np.random.seed(seed)
		dB = np.random.normal(loc=0.0, scale=np.sqrt(delta_t), size=(n_path_count, n_dim, n_step)).astype(np.float32) # Variance = delta_t

		for i in range(0, n_step):
			# Propagation using Euler
			X[:, :, i+1] = self.get_next(t0 + i * delta_t, X[:, :, i], delta_t, dB[:, :, i])
		
		return X, dB
		

############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Semilinear PDE with a known reference solution
# @returns Y, Z solutions. Shape should be [batch_size, d, n_timesteps].
class SemilinearPDE_ReferenceSolution(SemilinearPDE):
	
	@abc.abstractmethod
	def reference_solution(self, t, X):
		pass
	
	@abc.abstractmethod
	def reference_gradient(self, t, X):
		pass
	
	@abc.abstractmethod
	def reference_gradient_tf(self, t, X):
		pass


##
# @brief Semilinear PDE with a known MC solution
class SemilinearPDE_MCSolution(SemilinearPDE):
	
	@abc.abstractmethod
	def mc_solution(self, t, X, normal_realization=None):
		pass
	
	@abc.abstractmethod
	def mc_gradient(self, t, X, normal_realization=None):
		pass
	
	@abc.abstractmethod
	##
	# @brief Gets the reference solution from a file.
	# @returns Y, Z solutions. Shape should be [batch_size, d, n_timesteps].
	def get_solution_from_file(self, d, n_timesteps, X):
		pass


############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Implements Allen-Cahn equation.
# 
# u_t + laplacian(u)_x + u - u^3 = 0
# mu = [0, ..., 0]^T
# sigma * b = sqrt(2) * b
# f(..., Y, ...) = Y - Y^3
# g(..., X) = 1/(2 + 0.4 * norm(x)^2)
class AllenCahn_SigmaX(SemilinearPDE):
	
	def diff_np(self, t, X, delta_t):
		return 0.
	
	def vol_np(self, t, X, b):
		return np.multiply(0.2 * X, b)
		
	def f_tf(self, t, X, Y, Z):
		return Y - tf.pow(Y, 3)
		
	def g_tf(self, t, X):
		return 1. / (2. + 0.4 * tf.reduce_sum(tf.pow(X, 2), 1, keep_dims=True))


############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Implements HJB equation.
# 
# u_t + laplacian(u)_x + u - u^3 = 0
# mu = [0, ..., 0]^T
# sigma * b = sqrt(2) * b
# f(..., Y, ...) = - lambda * norm(grad(u))^2
# g(..., X) = log(0.5 * (1 + norm(x)^2))
class HamiltonJacobiBellman(SemilinearPDE_MCSolution):
	
	def __init__(self, a_lambd, a_T):
		self.lambd = a_lambd
		self.T = a_T
	
	def diff_np(self, t, X, delta_t):
		return 0.
	
	def vol_np(self, t, X, b):
		return np.sqrt(2) * b
	
	def vol_tf(self, t, X, b):
		return np.sqrt(2.) * b
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + np.sqrt(2.) * delta_b
		
	def f_tf(self, t, X, Y, Z):
		return - self.lambd * tf.reduce_sum(tf.pow(Z / np.sqrt(2.), 2), 1, keep_dims=True) # Have to divide by the volatility squared since Z = sigma^T grad u
		
	def g_tf(self, t, X):
		return tf.log(0.5 * (1 + tf.reduce_sum(tf.pow(X, 2), 1, keep_dims=True)))
	
	def g_np(self, t, X):
		return np.log(0.5 * (1 + np.sum(np.power(X, 2), 1, keepdims=True)))
	
	def g_p_np(self, t, X):
		return 2 * X /(1 + np.sum(np.power(X, 2), 1, keepdims=True))
	
	def monte_carlo_solution(self, t, X, T, normal_realization):
		
		if t == T:
			return self.g_np(T, X)
		
		tmp = np.exp(-self.lambd * self.g_np(t, np.expand_dims(X, axis=2) + np.sqrt(2) * np.sqrt(T-t) * normal_realization))
		return -(1/self.lambd) * np.log(np.mean(tmp, axis=2))
	
	def monte_carlo_gradient(self, t, X, T, normal_realization):
		
		if t == T:
			return self.g_p_np(T, X)
		
		tmp0 = np.exp(-self.lambd * self.g_np(t, np.expand_dims(X, axis=2) + np.sqrt(2) * np.sqrt(T-t) * normal_realization))
		tmp1 = np.multiply(self.g_p_np(t, np.expand_dims(X, axis=2) + np.sqrt(2) * np.sqrt(T-t) * normal_realization), tmp0)
		return np.divide(np.mean(tmp1, axis=2), np.mean(tmp0, axis=2))
	
	def mc_solution(self, t, X, normal_realization=None, n_monte_carlo=None):
		# Generate normal
		if normal_realization is None:
			n_monte_carlo= n_monte_carlo or _NUM_MC_RANDOM_SAMPLES
			normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
		return self.monte_carlo_solution(t, X, self.T, normal_realization)
		
	def mc_gradient(self, t, X, normal_realization=None, n_monte_carlo=None):
		# Generate normal
		if normal_realization is None:
			n_monte_carlo= n_monte_carlo or _NUM_MC_RANDOM_SAMPLES
			normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
		return self.monte_carlo_gradient(t, X, self.T, normal_realization)
	
	def get_solution_from_file(self, d, n_timesteps, X, override_mc_size=None, override_n_threads=1):
		# Considering a set of parameters, try to read a solution file
		# If nonexisting, compute the solution and save it
		
		# s is a combination of the parameters
		s = str(self.lambd) + str(self.T) + str(d) + str(n_timesteps)
		
		print("s", s)
		m = hashlib.md5()
		m.update(s.encode('utf-8'))
		hashstr = str(m.hexdigest())
		
		print("hashstr", hashstr)
		
		if override_mc_size is None:
			Y_filename = "./mc_solutions/" + "HJB_Y_" + hashstr
			Z_filename = "./mc_solutions/" + "HJB_Z_" + hashstr
		else:
			Y_filename = "./mc_solutions/" + "HJB_Y_" + hashstr + "_" + str(override_mc_size)
			Z_filename = "./mc_solutions/" + "HJB_Z_" + hashstr + "_" + str(override_mc_size)
		
		nb_realizations = X.shape[0]
		
		# Try to read the file
		Ysol = np.zeros((nb_realizations, 1, n_timesteps+1), dtype=np.float32)
		Zsol = np.zeros((nb_realizations, d, n_timesteps), dtype=np.float32)
		
		try:
			Ysol[:, 0, :] = np.loadtxt(Y_filename, dtype=np.float32)
			for i in range(d):
				Zsol[:, i, :] = np.loadtxt(Z_filename + "_" + str(i), dtype=np.float32)
		except:
			
			print("Generating solution")
			# Get MC solution
			
			delta_t = (self.T + 0.0) / n_timesteps
			tstamps = 0.0 + delta_t * np.arange(n_timesteps+1)
			
			if override_mc_size is None:
				n_threads=_MULTIPROCESSING_CORE_COUNT
				print("n_threads", n_threads)
				n_monte_carlo=_NUM_MC_RANDOM_SAMPLES
				normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
			else:
				n_threads=override_n_threads
				print("n_threads", n_threads)
				n_monte_carlo=override_mc_size
				normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
			
			res = Parallel(n_jobs=n_threads, verbose=1, backend="threading")(delayed(self.mc_solution)(tstamps[i], X[:, :, i], normal_realization) for i in range(n_timesteps+1))
			
			for i in range(n_timesteps+1):
				Ysol[:, :, i] = res[i]
			
			res = Parallel(n_jobs=n_threads, verbose=1, backend="threading")(delayed(self.mc_gradient)(tstamps[i], X[:, :, i], normal_realization) for i in range(n_timesteps))
			
			for i in range(n_timesteps):
				t = tstamps[i]
				Zsol[:, :, i] = self.vol_np(t, X[:, :, i], res[i])
			
			print("Saving solution")
			np.savetxt(Y_filename, Ysol[:, 0, :], header="lambda=" + str(self.lambd) + ", T=" + str(self.T) + ", d=" + str(d) + ", n_timesteps=" + str(n_timesteps))
			for i in range(d):
				np.savetxt(Z_filename + "_" + str(i), Zsol[:, i, :], header="lambda=" + str(self.lambd) + ", T=" + str(self.T) + ", d=" + str(d) + ", n_timesteps=" + str(n_timesteps))
			
		return Ysol, Zsol

	
	def monte_carlo_solution(self, t, X, T, normal_realization):
		
		if t == T:
			return self.g_np(T, X)
		
		tmp = np.exp(-self.lambd * self.g_np(t, np.expand_dims(X, axis=2) + np.sqrt(2) * np.sqrt(T-t) * normal_realization))
		return -(1/self.lambd) * np.log(np.mean(tmp, axis=2))
	
	def monte_carlo_gradient(self, t, X, T, normal_realization):
		
		if t == T:
			return self.g_p_np(T, X)
		
		tmp0 = np.exp(-self.lambd * self.g_np(t, np.expand_dims(X, axis=2) + np.sqrt(2) * np.sqrt(T-t) * normal_realization))
		tmp1 = np.multiply(self.g_p_np(t, np.expand_dims(X, axis=2) + np.sqrt(2) * np.sqrt(T-t) * normal_realization), tmp0)
		return np.divide(np.mean(tmp1, axis=2), np.mean(tmp0, axis=2))
	
	def mc_solution(self, t, X, normal_realization=None, n_monte_carlo=None):
		# Generate normal
		if normal_realization is None:
			n_monte_carlo= n_monte_carlo or _NUM_MC_RANDOM_SAMPLES
			normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
		return self.monte_carlo_solution(t, X, self.T, normal_realization)
		
	def mc_gradient(self, t, X, normal_realization=None, n_monte_carlo=None):
		# Generate normal
		if normal_realization is None:
			n_monte_carlo= n_monte_carlo or _NUM_MC_RANDOM_SAMPLES
			normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
		return self.monte_carlo_gradient(t, X, self.T, normal_realization)
	
	def get_solution_from_file(self, d, n_timesteps, X, override_mc_size=None, override_n_threads=1):
		# Considering a set of parameters, try to read a solution file
		# If nonexisting, compute the solution and save it
		
		# s is a combination of the parameters
		s = str(self.lambd) + str(self.T) + str(d) + str(n_timesteps)
		
		print("s", s)
		m = hashlib.md5()
		m.update(s.encode('utf-8'))
		hashstr = str(m.hexdigest())
		
		print("hashstr", hashstr)
		
		if override_mc_size is None:
			Y_filename = "./mc_solutions/" + "HJB_Y_" + hashstr
			Z_filename = "./mc_solutions/" + "HJB_Z_" + hashstr
		else:
			Y_filename = "./mc_solutions/" + "HJB_Y_" + hashstr + "_" + str(override_mc_size)
			Z_filename = "./mc_solutions/" + "HJB_Z_" + hashstr + "_" + str(override_mc_size)
		
		nb_realizations = X.shape[0]
		
		# Try to read the file
		Ysol = np.zeros((nb_realizations, 1, n_timesteps+1), dtype=np.float32)
		Zsol = np.zeros((nb_realizations, d, n_timesteps), dtype=np.float32)
		
		try:
			Ysol[:, 0, :] = np.loadtxt(Y_filename, dtype=np.float32)
			for i in range(d):
				Zsol[:, i, :] = np.loadtxt(Z_filename + "_" + str(i), dtype=np.float32)
		except:
			
			print("Generating solution")
			# Get MC solution
			
			delta_t = (self.T + 0.0) / n_timesteps
			tstamps = 0.0 + delta_t * np.arange(n_timesteps+1)
			
			
			if override_mc_size is None:
				n_threads=_MULTIPROCESSING_CORE_COUNT
				print("n_threads", n_threads)
				n_monte_carlo=_NUM_MC_RANDOM_SAMPLES
				normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
			else:
				n_threads=override_n_threads
				print("n_threads", n_threads)
				n_monte_carlo=override_mc_size
				normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
			
			res = Parallel(n_jobs=n_threads, verbose=1, backend="threading")(delayed(self.mc_solution)(tstamps[i], X[:, :, i], normal_realization) for i in range(n_timesteps+1))
			
			for i in range(n_timesteps+1):
				Ysol[:, :, i] = res[i]
			
			res = Parallel(n_jobs=n_threads, verbose=1, backend="threading")(delayed(self.mc_gradient)(tstamps[i], X[:, :, i], normal_realization) for i in range(n_timesteps))
			
			for i in range(n_timesteps):
				t = tstamps[i]
				Zsol[:, :, i] = self.vol_np(t, X[:, :, i], res[i])
			
			
			print("Saving solution")
			np.savetxt(Y_filename, Ysol[:, 0, :], header="alpha=" + str(self.alpha) + ", T=" + str(self.T) + ", d=" + str(d) + ", n_timesteps=" + str(n_timesteps))
			for i in range(d):
				np.savetxt(Z_filename + "_" + str(i), Zsol[:, i, :], header="alpha=" + str(self.alpha) + ", T=" + str(self.T) + ", d=" + str(d) + ", n_timesteps=" + str(n_timesteps))
			
		return Ysol, Zsol
	
	
############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Implements Black-Scholes equation with default risk.
# 
# See 3.1 in Han, Jentzen, E
class BlackScholesDefaultRisk(SemilinearPDE):
		
	def __init__(self, delta, R, mu_bar, sigma_bar, v_h, v_l, gamma_h, gamma_l):
		self.delta = np.array(delta, np.float32)
		self.R = np.array(R, np.float32)
		self.mu_bar = np.array(mu_bar, np.float32)
		self.sigma_bar = np.array(sigma_bar, np.float32)
		self.v_h = np.array(v_h, np.float32)
		self.v_l = np.array(v_l, np.float32)
		self.gamma_h = np.array(gamma_h, np.float32)
		self.gamma_l = np.array(gamma_l, np.float32)
	
	def diff_np(self, t, X, delta_t):
		return 0
	
	def vol_np(self, t, X, b):
		return np.multiply(self.sigma_bar * X, b)
	
	def vol_tf(self, t, X, b):
		return tf.multiply(self.sigma_bar * X, b)
	
	def f_tf(self, t, X, Y, Z):
		return (self.mu_bar / self.sigma_bar) * tf.reduce_sum(Z, axis=1, keep_dims=True) -(1-self.delta) * tf.minimum(self.gamma_h, tf.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y

	def g_tf(self, t, X):
		return tf.reduce_min(X, 1, keep_dims=True)
	
	def g_np(self, t, X):
		return np.amin(X, axis=1, keepdims=True)



############################################################################################################	
############################################################################################################	
############################################################################################################




##
# @brief Implements Black-Scholes equation with default risk.
# 
# See 3.1 in Han, Jentzen, E
class BlackScholesDefaultRisk_nosat(SemilinearPDE):
		
	def __init__(self, delta, R, mu_bar, sigma_bar, v_h, v_l, gamma_h, gamma_l):
		self.delta = np.array(delta, np.float32)
		self.R = np.array(R, np.float32)
		self.mu_bar = np.array(mu_bar, np.float32)
		self.sigma_bar = np.array(sigma_bar, np.float32)
		self.v_h = np.array(v_h, np.float32)
		self.v_l = np.array(v_l, np.float32)
		self.gamma_h = np.array(gamma_h, np.float32)
		self.gamma_l = np.array(gamma_l, np.float32)
	
	def diff_np(self, t, X, delta_t):
		#~ return 0
		#~ return self.mu_bar * X * delta_t
		return self.mu_bar * X * delta_t
		# Overriden by get_next
		#~ pass
	
	def vol_np(self, t, X, b):
		#~ return np.multiply(self.sigma_bar * X, b)
		return np.multiply(self.sigma_bar * X, b)
		# Overriden by get_next
		#~ pass
	
	def vol_tf(self, t, X, b):
		#~ return tf.multiply(self.sigma_bar * X, b)
		return tf.multiply(self.sigma_bar * X, b)
		
	def vol_tf_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return tf.divide(b, self.sigma_bar * X)
		
	def vol_np_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return b / (self.sigma_bar * X)
	
	def get_next(self, t, X, delta_t, delta_b):
		return X * np.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b)
		
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X * tf.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b)
	
	#~ def f_tf(self, t, X, Y, Z):
		#~ return -(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#
	
	#~ def f_np(self, t, X, Y, Z):
		#~ return -(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#
	
	def f_tf(self, t, X, Y, Z):
		return -(1-self.delta) * tf.minimum(self.gamma_h, tf.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#
	
	def f_np(self, t, X, Y, Z):
		return -(1-self.delta) * np.minimum(self.gamma_h, np.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#

	
	def g_tf(self, t, X):
		return tf.maximum(tf.reduce_mean(X, 1, keep_dims=True) - 100., 0.) #tf.reduce_min(X, 1, keep_dims=True)#
	
	def g_np(self, t, X):
		return np.maximum(np.mean(X, 1, keepdims=True) - 100., 0.) #np.amin(X, axis=1, keepdims=True)#


############################################################################################################	
############################################################################################################	
############################################################################################################


##
# @brief Implements a Black-Scholes-Barenblatt equation from Raissi, 2018
class BlackScholesBarenblatt(SemilinearPDE_ReferenceSolution):
		
	def __init__(self, sigma_bar, r, T):
		self.sigma_bar = sigma_bar
		self.r = r
		self.T = T
	
	def diff_np(self, t, X, delta_t):
		return 0
	
	def vol_np(self, t, X, b):
		return np.multiply(self.sigma_bar * X, b)
	
	def vol_tf(self, t, X, b):
		return tf.multiply(self.sigma_bar * X, b)
	
	def f_tf(self, t, X, Y, Z):
		return - self.r * (Y - tf.reduce_sum(Z, axis=1, keep_dims=True) / self.sigma_bar)

	def f_tf_grad(self, t, X, Y, grad):
		return - self.r * (Y - tf.reduce_sum(tf.multiply(grad, X), axis=1, keep_dims=True))

	def g_tf(self, t, X):
		return tf.reduce_sum(tf.pow(X, 2), 1, keep_dims=True)
	
	def g_np(self, t, X):
		return np.sum(np.power(X, 2), 1, keepdims=True) 

	def g_p_np(self, t, X):
		return 2.0 * X
		
	def g_p_tf(self, t, X):
		return 2.0 * X

	def reference_solution(self, t, X):
		return np.exp((self.r + self.sigma_bar**2) * (self.T - t)) * self.g_np( t, X)
	
	def reference_gradient(self, t, X):
		return np.exp((self.r + self.sigma_bar**2) * (self.T - t)) * self.g_p_np( t, X)

	def reference_gradient_tf(self, t, X):
		return tf.cast(tf.exp((self.r + self.sigma_bar**2) * (self.T - t)), tf.float32) * self.g_p_tf( t, X)


############################################################################################################	
############################################################################################################	
############################################################################################################	


## 
# @brief Implements Black-Scholes equation with different interest rates for borrowing and lending.
# 
# See 4.4 in E, Han and Jentzen
class BlackScholesInterestRates(SemilinearPDE):
		
	def __init__(self, mu_bar, sigma_bar, R_l, R_b):
		self.mu_bar = np.array(mu_bar, np.float32)
		self.sigma_bar = np.array(sigma_bar, np.float32)
		self.R_l = np.array(R_l, np.float32)
		self.R_b = np.array(R_b, np.float32)
	
	def diff_np(self, t, X, delta_t):
		pass
	
	def vol_np(self, t, X, b):
		pass
	
	def vol_tf(self, t, X, b):
		return tf.multiply(self.sigma_bar * X, b)
	
	def get_next(self, t, X, delta_t, delta_b):
		return np.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t) * np.multiply(np.exp(self.sigma_bar * delta_b), X)
	
	def f_tf(self, t, X, Y, Z):
		sum_zi = tf.reduce_sum(Z, 1, keep_dims=True)
		return - self.R_l * Y - ((self.mu_bar - self.R_l)/self.sigma_bar) * sum_zi + (self.R_b - self.R_l) * tf.maximum(np.array(0., np.float32), sum_zi / self.sigma_bar - Y)
	
	def g_tf(self, t, X):
		max_x = tf.reduce_max(X, 1, keep_dims=True)
		return tf.maximum(max_x - 120., np.array(0., np.float32)) - 2. * tf.maximum(max_x - 150., np.array(0., np.float32))

	def g_np(self, t, X):
		max_x = np.amax(X, axis=1, keepdims=True)
		return np.maximum(max_x - 120., 0.) - 2 * np.maximum(max_x - 150., 0.)

############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Implements multidimensional Burgers-type PDEs with explicit solutions.
# 
# u_t + d^2/2 Hess_x u + (u - (2+d)/(2d)) (d sum_{i=1}^d u_{x_{i}}) = 0
# mu = [0, ..., 0]^T
# sigma * b = d/sqrt(2) * b
# f(..., Y, Z) = (Y - (2+d)/(2d)) * (sum_{i=1}^d Z_i)
# g(t, X) = exp(t + (1/d) * sum_{i=1}^d X_i) / (1 + exp(T + (1/d) * sum_{i=1}^d X_i)) 
class MultidimensionalBurgers(SemilinearPDE):
	
	def __init__(self, a_d):
		self.d = a_d
	
	def diff_np(self, t, X, delta_t):
		pass
	
	def vol_np(self, t, X, b):
		pass
	
	def vol_tf(self, t, X, b):
		return (self.d / np.sqrt(2.)) * b
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + (self.d / np.sqrt(2.)) * delta_b
		
	def f_tf(self, t, X, Y, Z):
		return tf.multiply(Y - (2.+self.d)/(2.*self.d), tf.reduce_sum(Z, 1, keep_dims=True))
		
	def g_tf(self, t, X):
		tmp = t + (1./self.d) * tf.reduce_sum(X, 1, keep_dims=True)
		return tf.divide(tf.exp(tmp), 1. + tf.exp(tmp))

	def g_np(self, t, X):
		tmp = t + (1./self.d) * np.sum(X, axis=1, keepdims=True)
		return np.exp(tmp) / (1 + np.exp(tmp))

############################################################################################################	
############################################################################################################	
############################################################################################################	


class FirstToyExample(SemilinearPDE_ReferenceSolution):
	
	def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		self.mu_zero = a_mu_zero
		self.sigma_zero = a_sigma_zero
		self.d = a_d
		self.T = a_T
		self.a = a_a
		self.r = a_r
	
	def diff_np(self, t, X, delta_t):
		return self.mu_zero * delta_t / self.d
	
	def vol_np(self, t, X, b):
		return self.sigma_zero * b / np.sqrt(self.d)
	
	def vol_tf(self, t, X, b):
		return self.sigma_zero * b / np.sqrt(self.d)
	
	def f_tf(self, t, X, Y, Z):
		
		exp_a_T_m_t = tf.cast(tf.exp(self.a * (self.T - t)), tf.float32)
		sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		cos_sum_X = tf.cos(sum_X)
		sin_sum_X = tf.sin(sum_X)
		
		tmp1 = cos_sum_X * (self.a + self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = sin_sum_X * self.mu_zero * exp_a_T_m_t
		tmp3 = self.r * tf.pow(cos_sum_X, 2) * tf.pow(exp_a_T_m_t, 2)
		tmp4 = self.r * tf.pow(tf.maximum(-exp_a_T_m_t, tf.minimum(Y, exp_a_T_m_t)), 2)
		
		return tmp1 + tmp2 - tmp3 + tmp4
	
	def g_tf(self, t, X):
		return tf.cos(tf.reduce_sum(X, 1, keep_dims=True))
	
	def g_np(self, t, X):
		return np.cos(np.sum(X, axis=1, keepdims=True))
		
	def reference_solution(self, t, X):
		return np.exp(self.a * (self.T - t)) * np.cos(np.sum(X, axis=1, keepdims=True))
	
	def reference_gradient(self, t, X):
		return np.tile(-np.exp(self.a * (self.T - t)) * np.sin(np.sum(X, axis=1, keepdims=True)), (1, X.shape[1]))
	
	def reference_gradient_tf(self, t, X):
		return tf.tile(-tf.cast(tf.exp(self.a * (self.T - t)), tf.float32)* tf.sin(tf.reduce_sum(X, axis=1, keep_dims=True)), [1, tf.shape(X)[1]])


############################################################################################################	
############################################################################################################	
############################################################################################################	


class SecondToyExample(SemilinearPDE_ReferenceSolution):
	
	def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		self.mu_zero = a_mu_zero
		self.sigma_zero = a_sigma_zero
		self.d = a_d
		self.T = a_T
		self.a = a_a
		self.r = a_r
	
	def diff_np(self, t, X, delta_t):
		return self.mu_zero * delta_t / self.d
	
	def diff_tf(self, t, X, delta_t):
		return self.mu_zero * delta_t / self.d
	
	def vol_np(self, t, X, b):
		return self.sigma_zero * b / np.sqrt(self.d)
	
	def vol_tf(self, t, X, b):
		return self.sigma_zero * b / np.sqrt(self.d)
	
	def f_tf(self, t, X, Y, Z):
		
		exp_a_T_m_t = tf.cast(tf.exp(self.a * (self.T - t)), tf.float32)
		exp_2_a_T_m_t = tf.pow(exp_a_T_m_t, 2)
		sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		sum_Z = tf.reduce_sum(Z, 1, keep_dims=True)
		cos_sum_X = tf.cos(sum_X)
		sin_sum_X = tf.sin(sum_X)
		
		tmp1 = cos_sum_X * (self.a + self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = sin_sum_X * self.mu_zero * exp_a_T_m_t
		tmp3 = self.r * tf.pow(exp_a_T_m_t, 4) * tf.pow(cos_sum_X * sin_sum_X, 2)
		tmp4 = self.r * tf.pow(tf.maximum(-exp_2_a_T_m_t, tf.minimum(Y * sum_Z / (self.sigma_zero * np.sqrt(self.d)), exp_2_a_T_m_t)), 2)
		
		return tmp1 + tmp2 - tmp3 + tmp4
	
	def f_np(self, t, X, Y, Z):
		
		exp_a_T_m_t = np.exp(self.a * (self.T - t))
		exp_2_a_T_m_t = np.power(exp_a_T_m_t, 2)
		sum_X = np.sum(X, 1, keepdims=True)
		sum_Z = np.sum(Z, 1, keepdims=True)
		
		tmp1 = np.cos(sum_X) * (self.a + self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = np.sin(sum_X) * self.mu_zero * exp_a_T_m_t
		tmp3 = self.r * np.power(exp_a_T_m_t, 4) * np.power(np.cos(sum_X) * np.sin(sum_X), 2)
		
		tmpmult = np.multiply(Y, sum_Z)
		
		tmp4 = self.r * np.power(np.maximum(-exp_2_a_T_m_t, np.minimum(tmpmult / (self.sigma_zero * np.sqrt(self.d)), exp_2_a_T_m_t)), 2)
		
		return tmp1 + tmp2 - tmp3 + tmp4
	
	def g_tf(self, t, X):
		return tf.cos(tf.reduce_sum(X, 1, keep_dims=True))
	
	def g_np(self, t, X):
		return np.cos(np.sum(X, axis=1, keepdims=True))
	
	def reference_solution(self, t, X):
		return np.exp(self.a * (self.T - t)) * np.cos(np.sum(X, axis=1, keepdims=True))
	
	def reference_gradient(self, t, X):
		return np.tile(-np.exp(self.a * (self.T - t)) * np.sin(np.sum(X, axis=1, keepdims=True)), (1, X.shape[1]))

	def reference_gradient_tf(self, t, X):
		return tf.tile(-tf.cast(tf.exp(self.a * (self.T - t)), tf.float32) * tf.sin(tf.reduce_sum(X, axis=1, keep_dims=True)), [1, tf.shape(X)[1]])

		
############################################################################################################	
############################################################################################################	
############################################################################################################	



class ToyExample_ysz(SemilinearPDE_ReferenceSolution):
	
	def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		self.mu_zero = a_mu_zero
		self.sigma_zero = a_sigma_zero
		self.d = a_d
		self.T = a_T
		self.a = a_a
		self.r = a_r
	
	def diff_np(self, t, X, delta_t):
		#~ return 0
		return self.mu_zero * delta_t / self.d
	
	def diff_tf(self, t, X, delta_t):
		#~ return 0
		return self.mu_zero * delta_t / self.d
	
	def vol_np(self, t, X, b):
		return self.sigma_zero * b
	
	def vol_tf(self, t, X, b):
		return self.sigma_zero * b
	
	def f_tf(self, t, X, Y, Z):
		
		exp_a_T_m_t = tf.cast(tf.exp(self.a * (self.T - t)), tf.float32)
		sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		sum_Z = tf.reduce_sum(Z, 1, keep_dims=True)
		cos_sum_X = tf.cos(sum_X)
		sin_sum_X = tf.sin(sum_X)
		
		#~ diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		#~ nonlinearity = self.r * Y * tf.maximum(tf.minimum(self.d / (tf.sign(sum_Z) * tf.maximum(tf.abs(sum_Z), 1e-1)), 1/exp_a_T_m_t), 1/(3 * exp_a_T_m_t))
		nonlinearity = self.r * Y * tf.maximum(tf.minimum(self.d / (tf.sign(sum_Z) * tf.maximum(tf.abs(sum_Z), 1e-1)), 1/exp_a_T_m_t), 1/(3 * exp_a_T_m_t))
		
		tmp0 = 2 * self.a * sum_X * exp_a_T_m_t
		tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = - self.mu_zero * (2 - sin_sum_X) * exp_a_T_m_t
		tmp3 = - (self.r / self.sigma_zero) * (2 * sum_X + cos_sum_X) / (2 - sin_sum_X)
		
		phi = tmp0 + tmp1 + tmp2 + tmp3
		
		#~ return diffusion + nonlinearity + phi
		return nonlinearity + phi
	
	def f_np(self, t, X, Y, Z):
		
		exp_a_T_m_t = np.exp(self.a * (self.T - t))
		sum_X = np.sum(X, 1, keepdims=True)
		sum_Z = np.sum(Z, 1, keepdims=True)
		
		#~ diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		nonlinearity = self.r * Y * np.maximum(np.minimum(self.d / (np.sign(sum_Z) * np.maximum(np.abs(sum_Z), 1e-1)), 1/exp_a_T_m_t), 1/(3 * exp_a_T_m_t))
		
		tmp0 = 2 * self.a * sum_X * exp_a_T_m_t
		tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = - self.mu_zero * (2 - sin_sum_X) * exp_a_T_m_t
		tmp3 = - (self.r / self.sigma_zero) * (2 * sum_X + cos_sum_X) / (2 - sin_sum_X)
		
		phi = tmp0 + tmp1 + tmp2 + tmp3
		
		#~ return diffusion + nonlinearity + phi
		return nonlinearity + phi
	
	def g_tf(self, t, X):
		sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		return 2 * sum_X + tf.cos(sum_X)
	
	def g_np(self, t, X):
		sum_X = np.sum(X, axis=1, keepdims=True)
		return 2 * sum_X + np.cos(sum_X)
	
	def reference_solution(self, t, X):
		sum_X = np.sum(X, axis=1, keepdims=True)
		return np.exp(self.a * (self.T - t)) * (2 * sum_X + np.cos(sum_X))
	
	def reference_gradient(self, t, X):
		return np.tile(np.exp(self.a * (self.T - t)) * (2 - np.sin(np.sum(X, axis=1, keepdims=True))), (1, X.shape[1]))

	def reference_gradient_tf(self, t, X):
		return tf.tile(tf.cast(tf.exp(self.a * (self.T - t)), tf.float32) * (2 - tf.sin(tf.reduce_sum(X, axis=1, keep_dims=True))), [1, tf.shape(X)[1]])


############################################################################################################	
############################################################################################################	
############################################################################################################


class RichouToyExample(SemilinearPDE_MCSolution):
	
	def __init__(self, a_alpha, a_T):
		self.alpha = a_alpha
		self.T = a_T
	
	def diff_np(self, t, X, delta_t):
		return 0.
	
	def vol_np(self, t, X, b):
		return b
	
	def vol_tf(self, t, X, b):
		return b
	
	def f_tf(self, t, X, Y, Z):
		return tf.reduce_sum(tf.pow(Z, 2), axis=1, keep_dims=True) / 2.
	
	def f_np(self, t, X, Y, Z):
		return np.sum(np.power(Z, 2), axis=1, keepdims=True) / 2.
	
	def g_tf(self, t, X):
		X = tf.clip_by_value(X, 0., 1.)
		return tf.reduce_sum(tf.pow(X, self.alpha), axis=1, keep_dims=True)
	
	def g_np(self, t, X):
		return np.sum(np.power(np.clip(X, 0., 1.), self.alpha), axis=1, keepdims=True)
	
	def g_p_np(self, t, X):
		gp = np.zeros(X.shape)
		idx = np.where(np.logical_and(X > 0, X < 1))
		gp[idx] = self.alpha * np.power(X[idx], self.alpha-1)
		return gp
	
	def monte_carlo_solution(self, t, X, T, normal_realization):
		
		if t == T:
			return self.g_np(T, X)
		
		tmp = np.exp(self.g_np(t, np.expand_dims(X, axis=2) + np.sqrt(T-t) * normal_realization))
		return np.log(np.mean(tmp, axis=2))
	
	def monte_carlo_gradient(self, t, X, T, normal_realization):
		
		if t == T:
			return self.g_p_np(T, X)
		
		tmp0 = np.exp(self.g_np(t, np.expand_dims(X, axis=2) + np.sqrt(T-t) * normal_realization))
		tmp1 = np.multiply(self.g_p_np(t, np.expand_dims(X, axis=2) + np.sqrt(T-t) * normal_realization), tmp0)
		return np.divide(np.mean(tmp1, axis=2), np.mean(tmp0, axis=2))
	
	def mc_solution(self, t, X, normal_realization=None, n_monte_carlo=None):
		# Generate normal
		if normal_realization is None:
			n_monte_carlo= n_monte_carlo or _NUM_MC_RANDOM_SAMPLES
			normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
		return self.monte_carlo_solution(t, X, self.T, normal_realization)
		
	def mc_gradient(self, t, X, normal_realization=None, n_monte_carlo=None):
		# Generate normal
		if normal_realization is None:
			n_monte_carlo= n_monte_carlo or _NUM_MC_RANDOM_SAMPLES
			normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
		return self.monte_carlo_gradient(t, X, self.T, normal_realization)
	
	def get_solution_from_file(self, d, n_timesteps, X, override_mc_size=None, override_n_threads=1):
		# Considering a set of parameters, try to read a solution file
		# If nonexisting, compute the solution and save it
		
		# s is a combination of the parameters
		s = str(self.alpha) + str(self.T) + str(d) + str(n_timesteps)
		
		print("s", s)
		m = hashlib.md5()
		m.update(s.encode('utf-8'))
		hashstr = str(m.hexdigest())
		
		print("hashstr", hashstr)
		
		if override_mc_size is None:
			Y_filename = "./mc_solutions/" + "RichouToyExample_Y_" + hashstr
			Z_filename = "./mc_solutions/" + "RichouToyExample_Z_" + hashstr
		else:
			Y_filename = "./mc_solutions/" + "RichouToyExample_Y_" + hashstr + "_" + str(override_mc_size)
			Z_filename = "./mc_solutions/" + "RichouToyExample_Z_" + hashstr + "_" + str(override_mc_size)
		
		nb_realizations = X.shape[0]
		
		# Try to read the file
		Ysol = np.zeros((nb_realizations, 1, n_timesteps+1), dtype=np.float32)
		Zsol = np.zeros((nb_realizations, d, n_timesteps), dtype=np.float32)
		
		try:
			Ysol[:, 0, :] = np.loadtxt(Y_filename, dtype=np.float32)
			for i in range(d):
				Zsol[:, i, :] = np.loadtxt(Z_filename + "_" + str(i), dtype=np.float32)
		except:
			
			print("Generating solution")
			# Get MC solution
			
			delta_t = (self.T + 0.0) / n_timesteps
			tstamps = 0.0 + delta_t * np.arange(n_timesteps+1)
			
			
			if override_mc_size is None:
				n_threads=_MULTIPROCESSING_CORE_COUNT
				print("n_threads", n_threads)
				n_monte_carlo=_NUM_MC_RANDOM_SAMPLES
				normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
			else:
				n_threads=override_n_threads
				print("n_threads", n_threads)
				n_monte_carlo=override_mc_size
				normal_realization = np.random.normal(0.0, 1.0, size=(1, X.shape[1], n_monte_carlo))
			
			res = Parallel(n_jobs=n_threads, verbose=1, backend="threading")(delayed(self.mc_solution)(tstamps[i], X[:, :, i], normal_realization) for i in range(n_timesteps+1))
			
			for i in range(n_timesteps+1):
				Ysol[:, :, i] = res[i]
			
			res = Parallel(n_jobs=n_threads, verbose=1, backend="threading")(delayed(self.mc_gradient)(tstamps[i], X[:, :, i], normal_realization) for i in range(n_timesteps))
			
			for i in range(n_timesteps):
				t = tstamps[i]
				Zsol[:, :, i] = self.vol_np(t, X[:, :, i], res[i])
			
			print("Saving solution")
			np.savetxt(Y_filename, Ysol[:, 0, :], header="alpha=" + str(self.alpha) + ", T=" + str(self.T) + ", d=" + str(d) + ", n_timesteps=" + str(n_timesteps))
			for i in range(d):
				np.savetxt(Z_filename + "_" + str(i), Zsol[:, i, :], header="alpha=" + str(self.alpha) + ", T=" + str(self.T) + ", d=" + str(d) + ", n_timesteps=" + str(n_timesteps))
			
		return Ysol, Zsol



############################################################################################################	
############################################################################################################	
############################################################################################################



class CIRToy(SemilinearPDE_ReferenceSolution):
		
	def __init__(self, a, alpha, k, m, sigma, d, T):
		self.a = a
		self.alpha = alpha
		self.T = T,
		self.k = k
		self.m = m
		self.sigma = sigma
		self.d = d
	
	def diff_np(self, t, X, delta_t):
		return self.k * (self.m - X) * delta_t
	
	def vol_np(self, t, X, b):
		return self.sigma * np.multiply(np.sqrt(np.maximum(1e-8, X)), b)
	
	def vol_tf(self, t, X, b):
		return self.sigma * tf.multiply(tf.sqrt(tf.maximum(1e-8, X)), b)
	
	def f_tf(self, t, X, Y, Z):
		sum_X = tf.reduce_sum(X, axis=1, keep_dims=True)
		cos_sum_X = tf.cos(sum_X)
		sin_sum_X = tf.sin(sum_X)
		expTmt = tf.cast(tf.exp(-self.alpha * (self.T - t)), tf.float32)
		
		tmp1 = self.a * Y * tf.reduce_sum(Z, axis=1, keep_dims=True)
		tmp2 = (-self.alpha + self.sigma**2/2 * tf.reduce_sum(X, axis=1, keep_dims=True)) * cos_sum_X * expTmt
		tmp3 = self.k * tf.reduce_sum(self.m - X, axis=1, keep_dims=True) * sin_sum_X * expTmt
		tmp4 = self.a * cos_sum_X * sin_sum_X * expTmt**2 * self.sigma * tf.reduce_sum(tf.sqrt(tf.maximum(1e-8, X)), axis=1, keep_dims=True)
		return tmp1 + tmp2 + tmp3 + tmp4

	def g_tf(self, t, X):
		return tf.cos(tf.reduce_sum(X, 1, keep_dims=True))
	
	def g_np(self, t, X):
		return np.cos(np.sum(X, 1, keepdims=True))

	def reference_solution(self, t, X):
		return np.cos(np.sum(X, 1, keepdims=True)) * np.exp(-self.alpha * (self.T - t))
	
	def reference_gradient(self, t, X):
		return np.tile(-np.sin(np.sum(X, 1, keepdims=True)) * np.exp(-self.alpha * (self.T - t)), (1, X.shape[1]))

	def reference_gradient_tf(self, t, X):
		return tf.tile(-tf.sin(tf.reduce_sum(X, 1, keep_dims=True)) * tf.cast(tf.exp(-self.alpha * (self.T - t)), tf.float32), [1, tf.shape(X)[1]])
