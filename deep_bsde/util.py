##
# Implements auxiliary functions
# From https://arxiv.org/abs/1809.07609
#

import numpy as np
import time
import os
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

from adjustText import adjust_text

import math

import glob

#~ def get_cmap(n, name='Oranges'):
def get_cmap(n, name='Paired'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, n)

def eigsorted(cov):
    vals, vecs = np.linalg.eigh(cov)
    order = vals.argsort()[::-1]
    return vals[order], vecs[:,order]

def draw_confidence_ellipse(ax, x, y, color, ellipsis_std=2, alpha=0.15):
    nstd = ellipsis_std

    cov = np.cov(x, y)
    vals, vecs = eigsorted(cov)
    theta = np.degrees(np.arctan2(*vecs[:,0][::-1]))
    w, h = 2 * nstd * np.sqrt(vals)
    mean_x = np.mean(x)
    mean_y = np.mean(y)
    ell = Ellipse(xy=(mean_x, mean_y),
                  width=w, height=h,
                  angle=theta)
    ell.set_clip_box(ax.bbox)
    ell.set_alpha(alpha)
    ell.set_facecolor(color)
    ell.set_capstyle('projecting')
    ax.add_artist(ell)

    return mean_x, mean_y
    

def read_results_from_folder(folder_name, extra_fields={}, override_ref_Y0=None, override_ref_Z0_0=None):
    
    # Append a "/" if necessary
    if folder_name[-1] != "/":
        folder_name += "/"
    
    results = {}
    
    # Read best stats
    tmp = glob.glob(folder_name + "_best_stats.csv")
    fname= tmp[0]
    tmp = np.genfromtxt(fname, delimiter=',')
    results["best_loss"] = tmp[0]
    results["best_Y0"] = tmp[1]
    results["best_Z0_0"] = tmp[2]
    results["best_iter"] = int(tmp[3])
    results["nb_free_parameters"] = int(tmp[4])
    
    # Read losses
    tmp = glob.glob(folder_name + "losses.csv")
    fname= tmp[0]
    tmp = np.genfromtxt(fname, delimiter=',')
    results["error_Y0"] = tmp[0]
    results["error_Y0_abs"] = np.abs(tmp[0])
    results["error_Y_int_L1_mean"] = tmp[1]
    results["error_Y_int_L1_q5"] = tmp[2]
    results["error_Y_int_L1_q50"] = tmp[3]
    results["error_Y_int_L1_q95"] = tmp[4]
    results["error_Z_int_L2_mean"] = tmp[5]
    results["error_Z_int_L2_q5"] = tmp[6]
    results["error_Z_int_L2_q50"] = tmp[7]
    results["error_Z_int_L2_q95"] = tmp[8]
    
<<<<<<< Updated upstream
    try:
        # Read error on Z00
        results["reference_Y0"] = tmp[10]
        results["error_Y0_relative"] = results["error_Y0_abs"] / np.abs(results["reference_Y0"])
        results["error_Z0_0"] = tmp[9]
        results["error_Z0_0_abs"] = np.abs(results["error_Z0_0"])
        results["reference_Z0_0"] = tmp[11]
        results["error_Z0_0_relative"] = results["error_Z0_0_abs"] / np.abs(results["reference_Z0_0"])
        results["error_Z0_L2"] = tmp[12]
    except Exception as e:
        #print(e)
        pass
    
    #~ try:
        #~ results["reference_Z0_norm"] = tmp[13]
        #~ results["error_Z0_L2_relative"] = results["error_Z0_L2"] / results["reference_Z0_norm"]
    #~ except:
        #~ pass
    
=======
    # If references are overriden
>>>>>>> Stashed changes
    if override_ref_Y0 is not None:
        results["reference_Y0"] = override_ref_Y0
        results["error_Y0"] = results["best_Y0"] - override_ref_Y0
        results["error_Y0_abs"] = np.abs(results["error_Y0"])
        results["error_Y0_relative"] = results["error_Y0_abs"] / np.abs(results["reference_Y0"])
        #~ print("error_Y0_relative", results["error_Y0_relative"])
        #~ if math.isnan(results["error_Y0_relative"]):
            #~ print(folder_name)
    else:
        results["error_Y0_relative"] = results["error_Y0_abs"] / np.abs(results["best_Y0"])
    if override_ref_Z0_0 is not None:
        results["reference_Z0_0"] = override_ref_Z0_0
        results["error_Z0_0"] = results["best_Z0_0"] - override_ref_Z0_0
        results["error_Z0_0_abs"] = np.abs(results["error_Z0_0"])
        results["error_Z0_0_relative"] = results["error_Z0_0_abs"] / np.abs(results["reference_Z0_0"])
    
    # Read solution Z0
    tmp = glob.glob(folder_name + "Z0_ref_sol.csv")
    fname = tmp[0]
    tmp = np.array(np.genfromtxt(fname, delimiter=' '))
    #~ print("tmp", tmp)
    results["reference_Z0"] = tmp[1]
    results["solution_Z0"] = tmp[0]
    #~ print("results[\"solution_Z0\"]", results["solution_Z0"])
    
    try:
        results["error_Z0_L2_relative"] = np.power(results["error_Z0_L2"] / np.linalg.norm(results["solution_Z0"], ord=2), 2)
    except Exception as e:
        #~ print(e)
        pass
    
    # Read learning curves
    tmp = glob.glob(folder_name + "*_output_array.csv")
    fname = tmp[0]
    #print(fname)
    results["iter"] = np.array(np.genfromtxt(fname, delimiter=',')[:, 0], dtype=np.int32)
    results["loss_fun_iter"] = np.genfromtxt(fname, delimiter=',')[:, 1]
    results["Y0_fun_iter"] = np.genfromtxt(fname, delimiter=',')[:, 2]
    results["err_Y0_fun_iter"] = np.abs(results["Y0_fun_iter"] - results["reference_Y0"])
    results["Z0_0_fun_iter"] = np.genfromtxt(fname, delimiter=',')[:, 3]
    results["runtime_fun_iter"] = np.genfromtxt(fname, delimiter=',')[:, 4]
    results["runtime_100_iter"] = results["runtime_fun_iter"][-1] * 100 / results["iter"][-1]
    
    results["error_Y0_relative_fun_iter"] = np.abs(results["Y0_fun_iter"] - results["reference_Y0"]) / np.maximum(np.abs(results["reference_Y0"]), 1e-8)
    results["error_Z0_0_relative_fun_iter"] = np.abs(results["Z0_0_fun_iter"] - results["reference_Z0_0"]) / np.maximum(np.abs(results["reference_Z0_0"]), 1e-8)
    
    # Read final test loss
    tmp = glob.glob(folder_name + "final_test_loss.csv")
    fname = tmp[0]
    tmp = np.genfromtxt(fname, delimiter=',')
    results["final_test_loss"] = tmp
    
<<<<<<< Updated upstream
    if math.isnan(results["loss_fun_iter"][-1]):
        print("Unstable")
        results["stable"] = False
    else:
        results["stable"] = True
    
    
=======
>>>>>>> Stashed changes
    # Add extra fields
    results = {**results, **extra_fields}
    
    return results

def make_stats_for_experiments(result_list, field_name):
    
    num_experiments = len(result_list)
    try:
        result_length = len(result_list[0][field_name])
    except:
        result_length = 1
    
    result_array = np.zeros((result_length, num_experiments))
    for i in range(num_experiments):
        result_array[:, i] = result_list[i][field_name]
    
    result_stats = np.zeros((result_length, 4))
    
    result_stats[:, 0] = np.mean(result_array, axis=1)
    result_stats[:, 1] = np.percentile(result_array, 5, axis=1)
    result_stats[:, 2] = np.percentile(result_array, 50, axis=1)
    result_stats[:, 3] = np.percentile(result_array, 95, axis=1)
    
    return result_stats

def plot_mean_and_quantiles(ax, xdata, result_stats, color):
    
    variable_means = result_stats[:, 0]
    variable_5_quantile = result_stats[:, 1]
    variable_50_quantile = result_stats[:, 2]
    variable_95_quantile = result_stats[:, 3]
    
    # Plot means
    means_line, = ax.plot(xdata, variable_means, color=color)
    median_line, = ax.plot(xdata, variable_means, color=color, linestyle="dashed")
    
    # Fill 5 -> 95 quantiles
    quantile_area = ax.fill_between(xdata, variable_5_quantile, variable_95_quantile, where=variable_95_quantile >= variable_5_quantile, facecolor=color, alpha=0.3, interpolate=True)
    
    #~ return means_line, median_line, quantile_area
    return means_line, median_line, quantile_area

def plot_scatter_scalar_fields(ax, result_list, x_field_name, y_field_name, color, offset=0.0, draw_lines=False, linestyle='-', draw_scatter=False, marker="x", markersize=20, capsize=0, alpha=1.0, output_file_csv_name=None):
    
    number_of_points = len(result_list)
    
    scatter_points = np.zeros((number_of_points, 2))
    x_pts = []
    y_pts = []
    for i in range(number_of_points):
        scatter_points[i, 0] = result_list[i][x_field_name] + offset
        scatter_points[i, 1] = result_list[i][y_field_name]
        x_pts.append(result_list[i][x_field_name])
        y_pts.append(result_list[i][y_field_name])
    
    if draw_scatter:
        scatter_plot = ax.scatter(scatter_points[:, 0], scatter_points[:, 1], color=color, marker=marker, s=markersize)
    
    if draw_lines:
        # Plotting lines, y fun x
        # For each value of x
        x_unique = []
        y_unique = []
        yerr_low_unique = []
        yerr_high_unique = []
        for xvalue in np.sort(np.unique(x_pts)):
            x_unique.append(xvalue + offset)
            indices = np.where(np.equal(x_pts, xvalue))[0]
            #print(indices)
            
            y_mean = np.mean(np.array(y_pts)[indices])
            y_unique.append(y_mean)
            yerr_low_unique.append(y_mean - np.percentile(np.array(y_pts)[indices], 5))
            yerr_high_unique.append(np.percentile(np.array(y_pts)[indices], 95)- y_mean)
        
        #~ line_plot, = ax.plot(x_unique, y_unique, color=color, linestyle=linestyle)
        #~ scatter_plot = ax.scatter(x_unique, y_unique, color=color, marker=marker, s=markersize)
        
        yerrors = np.stack([yerr_low_unique, yerr_high_unique], axis=0)
        line_plot = ax.errorbar(x_unique, y_unique, yerr=yerrors, xerr=None, linestyle=linestyle, color=color, elinewidth=0.75, capsize=capsize, alpha=alpha)
        
    
        if output_file_csv_name:
            tmp_plot_data = np.zeros((len(np.sort(np.unique(x_pts))), 4))
            tmp_plot_data[:, 0] = np.array(np.sort(np.unique(x_pts)))
            tmp_plot_data[:, 1] = y_unique 
            tmp_plot_data[:, 2] = y_unique - np.array(yerr_low_unique)
            tmp_plot_data[:, 3] = np.array(yerr_high_unique) + y_unique
            
            np.savetxt(
                output_file_csv_name,
                tmp_plot_data,
                header="mean, q5, q95"
            )
    
    if draw_scatter:
        return scatter_plot
    return line_plot

def plot_all_lines(ax, result_list, x_field_name, y_field_name, legend_field_name=None):
    
    cmap = get_cmap(len(result_list))
    
    lines = []
    legends = []
    
    for i, res in enumerate(result_list):
        line, = ax.plot(res[x_field_name], res[y_field_name], color=cmap(i))
        lines.append(line)
        if legend_field_name is not None:
            legends.append(res[legend_field_name])
            
    if legend_field_name is not None:
        ax.legend(lines, legends)

def plot_scatter_field_with_annotation_and_ellipsis(ax, result_list, x_field_name, y_field_name, label_field_name, color, plot_label, plot_ellipsis=True, xytext=(-7, 7), ellipsis_std=2, linestyle='-', alpha=0.15, labelmod=0):
    
    # Sort the data for each value of label_field_name
    sorted_by_label = {}
    for res in result_list:
        res_label = res[label_field_name]
        if res_label not in sorted_by_label.keys():
            sorted_by_label[res_label] = []
        sorted_by_label[res_label].append(res)
    
    # Get x, y points for each label
    points_by_label = {}
    for label in sorted_by_label.keys():
        points_by_label[label] = [[], []]
        for res in sorted_by_label[label]:
            points_by_label[label][0].append(res[x_field_name]) # x coordinate
            points_by_label[label][1].append(res[y_field_name]) # y coordinate
    
    # Draw ellipses & get mean points
    labels = list(points_by_label.keys())
    mean_x_points = []
    mean_y_points = []
    for label in labels:
        if plot_ellipsis:
            mean_x, mean_y = draw_confidence_ellipse(ax, points_by_label[label][0], points_by_label[label][1], color, ellipsis_std, alpha)
        else:
            mean_x = np.mean(points_by_label[label][0])
            mean_y = np.mean(points_by_label[label][1])
        mean_x_points.append(mean_x)
        mean_y_points.append(mean_y)
    
    
    
    #~ print("labels", labels)
    
    # Sort
    indices = np.argsort(labels)
    #~ print(indices)
    labels = np.array(labels)[indices]
    mean_x_points = np.array(mean_x_points)[indices]
    mean_y_points = np.array(mean_y_points)[indices]
    
    # Draw arrow for the first 2 points
    #~ if len(labels) > 1:
        #~ ax.arrow(mean_x_points[0], mean_y_points[0], (mean_x_points[1] + mean_x_points[0]) / 2 - mean_x_points[0], (mean_y_points[1] + mean_y_points[0]) / 2 - mean_y_points[0], color=color, width=0.001)
    
    
    # Print labels
    if plot_label:
        for i, label in enumerate(labels):
            if i % 2 == labelmod:
                plt.annotate(
                    label,
                    xy=(mean_x_points[i], mean_y_points[i]), xytext=xytext,
                    textcoords='offset points', ha='right', va='bottom',
                    #bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
                    arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0')
                )
    #~ if plot_label:
        #~ texts = []
        #~ for x, y, s in zip(mean_x_points, mean_y_points, labels):
            #~ texts.append(ax.text(x, y, s))
        #~ adjust_text(texts, x=x, y=y, autoalign='y',
            #~ only_move={'points':'y', 'text':'y'}, force_points=0.15,
            #~ arrowprops=dict(arrowstyle="->", color='r', lw=0.5))
        #~ adjust_text(texts)#, only_move='y', arrowprops=dict(arrowstyle="->", color='r', lw=0.5))
    
    # Print line
    line, = ax.plot(mean_x_points, mean_y_points, color=color, linestyle=linestyle)
    #~ ax.scatter(mean_x_points, mean_y_points, marker='o', s=30, color=color)
    ax.scatter(mean_x_points, mean_y_points, marker='x', s=20, color=color)
    #~ for label in labels:
        #~ ax.scatter(points_by_label[label][0], points_by_label[label][1], marker='x', s=20, color=color)
    
    return line
    
