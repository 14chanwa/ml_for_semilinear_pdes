##
# Example use of our new algorithm on an example with square root terminal condition
# From https://arxiv.org/abs/1809.07609
#

import numpy as np
import tensorflow as tf
import deep_xavier as dnesting
import time
import os
import matplotlib.pyplot as plt


n_inner = 10000
print("n_inner", n_inner)
run_index = 1
print("run_index", run_index)
output_dir_name = "./output/richou_d10_deep_nesting"
print("output_dir_name", output_dir_name)


monte_carlo_size = 1024
min_decrease_rate = 0.05

d=10
case_name = "RichouToyExample"


T = 3.
X0 = np.zeros((d,), dtype=np.float64)
n_timesteps=100

pde = dnesting.RichouToyExample(
    a_alpha=0.5,
    a_T = T
)


print("----- Solving RichouToyExample -----")


sp = dnesting.XavierSolver.Parameters(
        pde,
        d=d,
        T=T,
        n_timesteps=n_timesteps,
        X0=X0,
        Yini=[.7, 1.3]
        )

sp.valid_size = monte_carlo_size

sp.learning_rate = 0.01
sp.use_predefined_learning_rate_strategy = False
sp.n_miniter = 16000
sp.n_maxiter = 16000

sp.batch_size = 300
sp.valid_size = 1000
sp.normalize_input_X = True

sp.num_hidden_layers = 3
sp.hidden_layer_size = 2*d
sp.activation_fn = tf.nn.tanh

sp.n_inner = n_inner
sp.lamb = 1.0
sp.t_distrib_a = 0.0

### Computing the MC solution with a lot of threads (server
### or use a precomputed MC solution (see README)
sp.override_mc=True
sp.override_mc_size=50000
sp.override_n_threads=1


output_dir_name_complete = output_dir_name + "Deep_nesting_" + str(n_inner) + "_ninner_" + str(run_index) + "/"
solver = dnesting.XavierSolver(sp, base_name=case_name, output_directory=output_dir_name_complete)

solver.train(run_name="Test", min_decrease_rate=min_decrease_rate)
